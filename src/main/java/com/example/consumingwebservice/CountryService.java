package com.example.consumingwebservice;


import java.util.HashMap;
import java.util.Map;

import com.example.consumingwebservice.wsdl.GetCountryRequest;
import com.example.consumingwebservice.wsdl.GetCountryResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.core.SoapActionCallback;

@Service
public class CountryService {

    private final WebServiceTemplate webServiceTemplate;

    @Autowired
    public CountryService(WebServiceTemplate webServiceTemplate) {
        this.webServiceTemplate = webServiceTemplate;
        Map<String, Object> props = new HashMap<String, Object>();
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        props.put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        marshaller.setMarshallerProperties(props);
        marshaller.setContextPath("com.example.consumingwebservice.wsdl");
        webServiceTemplate.setMarshaller(marshaller);
        webServiceTemplate.setUnmarshaller(marshaller);
    }

    public GetCountryResponse getCountry(String country) {
        GetCountryRequest request = new GetCountryRequest();
        request.setName(country);
        GetCountryResponse response = (GetCountryResponse) webServiceTemplate
                .marshalSendAndReceive("http://localhost:8015/ws/countries", request,
                        new SoapActionCallback(
                                "http://spring.io/guides/gs-producing-web-service/GetCountryRequest"));

        return response;
    }
}
