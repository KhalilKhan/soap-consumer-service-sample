package com.example.consumingwebservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.webservices.client.WebServiceTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ws.client.core.WebServiceTemplate;

@Configuration
public class WebServiceConfig {
    
  @Autowired
  private WebServiceTemplateBuilder builder;

  @Bean
  public WebServiceTemplate webServiceTemplate () {
    return builder.build();
  }
}
