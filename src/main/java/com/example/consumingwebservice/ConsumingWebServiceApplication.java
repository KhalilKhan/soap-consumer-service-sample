package com.example.consumingwebservice;

import javax.annotation.PostConstruct;

import com.example.consumingwebservice.wsdl.Country;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsumingWebServiceApplication {

	@Autowired
	private CountryService countryService;

	public static void main(String[] args) {
		SpringApplication.run(ConsumingWebServiceApplication.class, args);
	}

	@PostConstruct
	public void print(){
		Country country = countryService.getCountry("Pakistan").getCountry();
		System.out.println(country.getName());
		System.out.println(country.getCapital());
		System.out.println(country.getCurrency());
	}


}
